#version 120
attribute vec2 square;
uniform ivec2 screen_shape;

uniform ivec2 shape;
uniform ivec2 ll_pos;

varying vec2 s;

void main() 
{
    s = square;
    vec2 u = shape*square + ll_pos;
    gl_Position = vec4(2*u/screen_shape - 1, 0.0, 1.0);
}
