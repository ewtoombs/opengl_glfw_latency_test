#version 120

uniform sampler2D sampler;

varying vec2 s;

void main(void) {
    gl_FragColor = texture2D(sampler, s);
}
