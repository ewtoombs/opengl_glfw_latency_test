#version 120
attribute vec2 vertex_pos;

uniform ivec2 screen_shape;
uniform vec2 model_pos;

void main() 
{
    vec2 u = vertex_pos + model_pos;
    gl_Position = vec4(2*u/screen_shape - 1, 0.0, 1.0);
}
