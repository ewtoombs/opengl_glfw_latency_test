// vi:fo=qacj com=b\://

#pragma once

#include <GL/glew.h>  // needed for shaders and shit.
#include <GLFW/glfw3.h>

#define STATIC_LEN(a) (sizeof(a)/sizeof(*a))

char *load(const char *fn);

#ifdef	NDEBUG
# define gl_assert() ((void) (0))
#else
# define gl_assert() \
     (_gl_assert(__FILE__, __LINE__, __extension__ __PRETTY_FUNCTION__))
#endif
void _gl_assert(const char *file, unsigned int line, const char *function);

GLuint shader_program(const char *vertfile, const char *fragfile);
void process_events_for(double dt);
