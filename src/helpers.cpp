// vi:fo=qacj com=b\://

#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>

#include <complex>
#include <cmath>
using namespace std;

#include <GL/glew.h>  // needed for shaders and shit.
#include <GLFW/glfw3.h>

#include "helpers.hpp"


// Read a file to a string.
char *load(const char *fn)
{
    int fd = open(fn, O_RDONLY);
    assert(fd != -1);

    off_t size = lseek(fd, 0, SEEK_END);
    assert(size != -1);
    off_t res = lseek(fd, 0, SEEK_SET);
    assert(res != -1);

    size++;  // null terminator
    char *buf = (char *)malloc(size);

    char *p = buf;
    for (;;) {
        // File has gotten bigger since we started? Fuck that.
        assert(p - buf < size);
        ssize_t nread = read(fd, (char *)p, 0x10000);
        assert(nread != -1);
        if (nread == 0) {
            *p = '\0';
            break;
        }

#ifndef NDEBUG
        // Null character? Fuck that shit.
        void *nullbyte = memchr((char *)p, '\0', nread);
        assert(nullbyte == NULL);
#endif

        p += nread;
    }
    int cres = close(fd);
    assert(cres == 0);

    return buf;
}


// Do not call this function directly. Use the macro gl_assert(). Use it after
// an OpenGL API call to abort with a helpful error message if anything goes
// wrong. Example:
/// glCompileShader(shader); gl_assert();
void _gl_assert(const char *file, unsigned int line, const char *function)
{
    GLenum e = glGetError();
    if (e != GL_NO_ERROR) {
        fprintf(stderr, "GL assertion failed. file: %s\n"
                "function: %s\nline: %d     GL error: %s\n",
                file, function, line, gluErrorString(e));
        abort();
    }
}

// Compile the "type" shader named "filename" and attach it to
// "shader_program".
static void compile_shader(GLenum type, const char *filename,
        GLuint shader_program)
{
    GLuint shader = glCreateShader(type);
    char *source = load(filename);
    glShaderSource(shader, 1, &source, 0);
    glCompileShader(shader);
#ifndef NDEBUG
    GLint success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLint log_length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);
        char *log = (char *)malloc(log_length);
        glGetShaderInfoLog(shader, log_length, NULL, log);
        fprintf(stderr, "Failed to compile %s:\n%s", filename, log);
        abort();
    }
#endif
    glAttachShader(shader_program, shader);
}
// Return a shader program with vertex shader "vertfile" and the fragment
// shader "fragfile".
GLuint shader_program(const char *vertfile, const char *fragfile)
{
    GLuint p = glCreateProgram();
    compile_shader(GL_VERTEX_SHADER, vertfile, p);
    compile_shader(GL_FRAGMENT_SHADER, fragfile, p);
    //glLinkProgram(p);
    return p;
}

// Process events for dt seconds, then return. Should almost always return in
// exactly dt seconds.
void process_events_for(double dt)
{
    for (;;) {
        double t0 = glfwGetTime();
        glfwWaitEventsTimeout(dt);
        double u = glfwGetTime() - t0;
        if (u >= dt)
            return;
        dt -= u;
    }
}
