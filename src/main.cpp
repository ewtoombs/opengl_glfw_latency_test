// vi:fo=qacj com=b\://

#include <stdio.h>
#include <assert.h>

#include <vector>
using std::vector;

#include <GL/glew.h>  // needed for shaders and shit.
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WAYLAND
#include <GLFW/glfw3native.h>

#include "helpers.hpp"

// Screen dimension constants
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define SCREEN_RATIO ((float)SCREEN_WIDTH / (float)SCREEN_HEIGHT)

void error_callback(int error, const char* description);
bool init(void);
bool init_gl();
void key_callback(GLFWwindow *window, int key, int scancode,
        int action, int mods);
void cursor_position_callback(GLFWwindow *window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow *window, int button,
        int action, int mods);
void render(void);
bool tasting(void);
void pres_set(bool p);


// Globals, prefixed with g_.

// The cursor model. Use a small number, VEPS, to ensure pixels falling exactly
// on the triangles' lines are contained /inside/ the triangle, so that they
// get drawn.
#define VEPS 0x1p-8
float g_cursor_model[] = {
    12.5f      , 24.5f + VEPS,   16.5f + VEPS, 16.5f + VEPS,
     0.5f - VEPS,  0.5f - VEPS,   24.5f + VEPS, 12.5f      ,};

GLFWwindow *g_window;
GLuint g_blit_program;
float g_square_data[] = {0.f, 0.f,  1.f, 0.f,  0.f, 1.f,  1.f, 1.f};
unsigned g_square_len = STATIC_LEN(g_square_data)/2;

GLuint g_mesh2d_program;
GLuint g_model_pos_uni;

vector<float> g_cursor_model_baked;

GLuint g_cursors_vbo;
unsigned g_cursors_size = 0x100000;
unsigned g_cursors_len = 0;

unsigned char g_frame_counter = 0;
unsigned g_event_counter = 0;
unsigned g_delay = 0;

bool g_pres;


void error_callback(int error, const char *description)
{
    fprintf(stderr, "Error: %s\n", description);
}
// Starts up glfw, creates window, and initialises the glfw- and
// vendor-specific OpenGL state.
bool init(void)
{
    glfwSetErrorCallback(error_callback);

    if (!glfwInit())
        return false;


    // Make the window and the OpenGL context.

    // Set OpenGL version.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    g_window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "template",
            NULL, NULL);
    if (g_window == NULL)
        return false;
    glfwMakeContextCurrent(g_window);


    glfwSetKeyCallback(g_window, key_callback);
    glfwSetCursorPosCallback(g_window, cursor_position_callback);
    glfwSetMouseButtonCallback(g_window, mouse_button_callback);

    // Do the arcane OpenGL extension badness nobody but GLEW properly
    // understands.
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK) {
        printf("Error initialising GLEW! %s\n",
                glewGetErrorString(glewError));
    }

    pres_set(false);

    // Initialise OpenGL.
    if (!init_gl()) {
        printf("Unable to initialise OpenGL!\n");
        return false;
    }

    return true;
}

// Initialises the generic OpenGL state.
bool init_gl()
{
    // Repeat the first and last vertices of the cursor model, so that all
    // cursors can be drawn in a single draw call. Zero-width "triangles" get
    // "drawn" between the cursors.
    unsigned len = STATIC_LEN(g_cursor_model);
    g_cursor_model_baked.push_back(g_cursor_model[0]);
    g_cursor_model_baked.push_back(g_cursor_model[1]);
    for (unsigned i = 0; i < len; i++)
        g_cursor_model_baked.push_back(g_cursor_model[i]);
    g_cursor_model_baked.push_back(g_cursor_model[len - 2]);
    g_cursor_model_baked.push_back(g_cursor_model[len - 1]);
    printf("len = %d\n", len);
    printf("baked len = %d\n", g_cursor_model_baked.size());

    // First, a little OpenGL terminology. One draw call draws an array of
    // primitives onto the framebuffer.  A primitive has three types: a point,
    // a line, or a triangle.  The primitives drawn by a single call are all of
    // the same type. A VAO stores the metadata (type, size, attribute index)
    // of multiple VBOs.  A VBO stores data of any kind in video memory.

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Make the VBO that will store all of the cursor images.
    glGenBuffers(1, &g_cursors_vbo);
    // Tell the GL to use cursor_model_vbo next time a VBO is needed to
    // provide data to a vertex attribute array.
    glBindBuffer(GL_ARRAY_BUFFER, g_cursors_vbo);
    // Upload the vertex data in background_data to the video device.
    glBufferData(GL_ARRAY_BUFFER, g_cursors_size, NULL, GL_DYNAMIC_DRAW);
    GLuint attr = 0;
    glEnableVertexAttribArray(attr);
    // Associate the currently-bound VBO with the vertex attribute at index
    // attr.  Also, tell the GL what data type it is, because this is not fully
    // specified in the vertex shader's source file.  For instance, the 'float'
    // in GLSL does not necessarily correspond to the C 'float', and indeed may
    // even vary between graphics card models.
    glVertexAttribPointer(attr, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    // The shader program is called once per draw call.  It consists of a
    // vertex shader and a fragment shader.  (One fragment is basically a
    // pixel.  Specifically it is a pixel that has been generated by the
    // rasterisation of a primitive. All of the pixels aren't fragments. In
    // this way, one fragment is one piece of a primitive.) The vertex program
    // is called in parallel, once per vertex.  The fragment shader is also
    // called in parallel, but once per fragment. 
    g_mesh2d_program = shader_program("glsl/2d.vert", "glsl/white.frag");
    // Pass the cursor model data to the "vertex_pos" input of the vertex
    // shader.  This will associate one 2-vector out of cursor_model_data with
    // every vertex the vertex shader processes. That 2-vector gets accessed by
    // the name "vertex_pos" in 2d.vert. The data sent to the vertex shader
    // does not need to have any specific purpose or data type.  It is up to
    // the vertex shader to figure out how to turn that data into a vertex
    // position.
    glBindAttribLocation(g_mesh2d_program, attr, "vertex_pos");
    glLinkProgram(g_mesh2d_program);
    // Use the 2d shader program in all subsequent draw calls. Must be done
    // before the program's inputs may be set with calls to glUniform* and
    // glVertexAttribPointer.
    glUseProgram(g_mesh2d_program);

    // For all subsequent draw calls, pass SCREEN_{WIDTH,HEIGHT} into the
    // uniform vertex shader input, screen_shape. Uniform means every vertex
    // gets the same value of this input.
    glUniform2i(glGetUniformLocation(g_mesh2d_program, "screen_shape"),
            SCREEN_WIDTH, SCREEN_HEIGHT);

    glUniform2f(glGetUniformLocation(g_mesh2d_program, "model_pos"), 0, 0);



    // Repeat for the blitter.
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_square_data),
            g_square_data, GL_STATIC_DRAW);
    attr++;
    glEnableVertexAttribArray(attr);
    glVertexAttribPointer(attr, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    g_blit_program = shader_program("glsl/blit.vert", "glsl/blit.frag");
    glBindAttribLocation(g_blit_program, attr, "square");
    glLinkProgram(g_blit_program);
    glUseProgram(g_blit_program);


    glUniform2i(glGetUniformLocation(g_blit_program, "screen_shape"),
            SCREEN_WIDTH, SCREEN_HEIGHT);
    // Sampler2d-typed variables in GLSL can't be specified with literals, so
    // pass 0 through this uniform to tell the shader to use the default
    // texture unit, GL_TEXTURE0.
    glUniform1i(glGetUniformLocation(g_blit_program, "sampler"), 0);

    unsigned width = 250, height = 200;
    glUniform2i(glGetUniformLocation(g_blit_program, "shape"), width, height);
    glUniform2i(glGetUniformLocation(g_blit_program, "ll_pos"),
            SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    uint32_t tex[width*height];
    for (int i = 0; i < width; i++)
    for (int j = 0; j < height; j++)
        tex[i + width*j] = 0xffffffff*((i ^ j)&1);
    // The texture will load as opaque black unless GL_TEXTURE_MIN_FILTER is
    // set to GL_NEAREST or GL_LINEAR. This is because its default state only
    // works when the texture's entire mipmap set is populated and we only load
    // level 0. Anyway, this is something we should change anyway, because we
    // aren't interpolating anything.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // This one doesn't need to be changed to get it to work, but we should
    // anyway, because again, we are blitting without interpolation. Keep those
    // pixels pristine.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, tex);


    // Set the colour to be used in all subsequent glClear(GL_COLOR_BUFFER_BIT)
    // commands.
    glClearColor(0.f, 0.f, 0.f, 1.f);

    glBindBuffer(GL_ARRAY_BUFFER, g_cursors_vbo);

    return true;
}

// Per-frame actions.
void render(void)
{
    glUseProgram(g_blit_program);
    // Draw lines with the active shader program and its current inputs.
    glDrawArrays(GL_TRIANGLE_STRIP, 0, g_square_len);

    glUseProgram(g_mesh2d_program);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, g_cursors_len);

}


void key_callback(GLFWwindow *window, int key, int scancode,
        int action, int mods)
{
    if (key == GLFW_KEY_Q && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    if (key == GLFW_KEY_E && action == GLFW_PRESS) {
        pres_set(!g_pres);
    }
    if (key == GLFW_KEY_A && action == GLFW_PRESS) {
        g_delay = (g_delay - 1)%16;
        printf("Delay reduced   to %2d ms.\n", g_delay);
    }
    if (key == GLFW_KEY_D && action == GLFW_PRESS) {
        g_delay = (g_delay + 1)%16;
        printf("Delay increased to %2d ms.\n", g_delay);
    }
}
void cursor_position_callback(GLFWwindow *window, double xpos, double ypos)
{
    unsigned len = g_cursor_model_baked.size();
    vector<float> data = g_cursor_model_baked;
    for (unsigned i = 0; i < len; i += 2) {
        data[i] += xpos;
        data[i+1] += SCREEN_HEIGHT - ypos;
    }
    g_cursors_len = 0;
    glBufferSubData(GL_ARRAY_BUFFER, g_cursors_len*2*sizeof(float),
            len*sizeof(float), data.data());
    g_cursors_len += len/2;

    g_event_counter++;
}
void mouse_button_callback(GLFWwindow *window, int button,
        int action, int mods)
{
    if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT) {
    }
    if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_RIGHT) {
    }
    if (action == GLFW_RELEASE && button == GLFW_MOUSE_BUTTON_LEFT) {
    }
    if (action == GLFW_RELEASE && button == GLFW_MOUSE_BUTTON_RIGHT) {
    }
}

void pres_set(bool p)
{
    g_pres = p;
    if (g_pres) {
        glfwSwapInterval(0);
        puts("Now synchronising with presentation-time.");
    } else {
        // Turn on double buffering and vsync. 1 is the minimum time in frames
        // to wait after a vsync before swapping buffers.
        glfwSwapInterval(1);
        puts("Now synchronising with glfwSwapInterval(1).");
    }
}


// Give the stew a taste every once in a while.
bool about_to_taste(void)
{
    return g_frame_counter == 0xff;
}
bool tasting(void)
{
    return g_frame_counter == 0;
}
int main(int argc, char *argv[])
{
    // Start up glfw and create window.
    if (!init()) {
        printf("Failed to initialise!\n");
    } else {
        const GLFWvidmode *m = glfwGetVideoMode(glfwGetPrimaryMonitor());
        double T = 1. / (double)m->refreshRate;
        printf("T = %.3fms\n", T*1000.);

        double t_last_frame = glfwGetTime();
        while (!glfwWindowShouldClose(g_window)) {  // once per frame.
            double t;
            double t_pres;
            if (tasting())
                t = glfwGetTime();

            if (g_pres)
                t_pres = glfwWaylandSwapPres(g_window);
            else
                glfwSwapBuffers(g_window);
            //---------------- ***VSYNC*** ----------------

            // OK, the vsync has like /juuuust/ happened. The buffers have just
            // been swapped for suresiez.
            if (about_to_taste()) {
                t_last_frame = glfwGetTime();
                g_event_counter = 0;
            }
            double t1;
            if (tasting()) {
                t1 = glfwGetTime();
                printf("Time waiting for vsync: %.3fms.\n"
                        "Frame duration: %.3fms\n", (t1 - t)*1000.,
                        (t1 - t_last_frame)*1000.);
                if (g_pres)
                    printf("Time elapsed since presentation time: %.3fms\n",
                            (t1 - t_pres)*1000.);
                fputc('\n', stdout);
            }
            g_frame_counter++;

            // Process events for g_delay milliseconds, to see what happens to
            // the latency when waiting for different amounts of time after the
            // buffer swap.
            if (tasting()) {
                t = glfwGetTime();
            }
            process_events_for(g_delay*1e-3);
            if (tasting())
                printf("process_events_for takes %.3fms out of %dms. "
                        "We saw %d events.\n",
                        (glfwGetTime() - t)*1000., g_delay,
                        g_event_counter);

            // We have awoken! It is only T_RENDER seconds before the next
            // vsync, and we have got a frame to render!  Do all OpenGL drawing
            // commands. 
            if (tasting())
                t = glfwGetTime();
            glClear(GL_COLOR_BUFFER_BIT);
            render();
            glFinish();
            if (tasting())
                printf("Draw takes %.3fms.\n", (glfwGetTime() - t)*1000.);
        }
    }

    // Destroy window
    glfwDestroyWindow(g_window);

    glfwTerminate();

    return 0;
}
